package org.acme.hello;

import junit.framework.TestCase;

public class HelloWorldTestCase extends TestCase
{
    public void setUp()
    {
        // Nothing to see here
    }
    
    public void testHello() throws Exception
    {
        Hello h = (Hello) Hello.class.newInstance();
        h.doSomething();
    }
}